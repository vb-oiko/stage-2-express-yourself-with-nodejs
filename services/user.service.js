const repo = require("../repositories/user.repository");

const getUsers = () => repo.getUsers();
const getUserById = id => repo.getUserById(id);
const createUser = user => repo.createUser(user);
const updateUser = (id, user) => repo.updateUser(id, user);
const deleteUser = id => repo.deleteUser(id);

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
};
