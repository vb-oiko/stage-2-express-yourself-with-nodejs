const DATA = [
  {
    _id: "1",
    name: "Ryu",
    health: 45,
    attack: 4,
    defense: 3,
    source: "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
  },
  {
    _id: "2",
    name: "Dhalsim",
    health: 60,
    attack: 3,
    defense: 1,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
  },
  {
    _id: "3",
    name: "Guile",
    health: 45,
    attack: 4,
    defense: 3,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif"
  },
  {
    _id: "4",
    name: "Zangief",
    health: 60,
    attack: 4,
    defense: 1,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif"
  },
  {
    _id: "5",
    name: "Ken",
    health: 45,
    attack: 3,
    defense: 4,
    source: "http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif"
  },
  {
    _id: "6",
    name: "Bison",
    health: 45,
    attack: 5,
    defense: 4,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
  },
  {
    _id: "7",
    name: "Chun-Li",
    health: 40,
    attack: 3,
    defense: 8,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif"
  },
  {
    _id: "8",
    name: "Blanka",
    health: 80,
    attack: 8,
    defense: 2,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif"
  },
  {
    _id: "9",
    name: "E.Honda",
    health: 50,
    attack: 5,
    defense: 5,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif"
  },
  {
    _id: "10",
    name: "Balrog",
    health: 55,
    attack: 4,
    defense: 6,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif"
  },
  {
    _id: "11",
    name: "Vega",
    health: 50,
    attack: 4,
    defense: 7,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif"
  },
  {
    _id: "12",
    name: "Sagat",
    health: 55,
    attack: 4,
    defense: 6,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif"
  },
  {
    _id: "13",
    name: "Cammy",
    health: 45,
    attack: 4,
    defense: 7,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif"
  },
  {
    _id: "14",
    name: "Fei Long",
    health: 80,
    attack: 2,
    defense: 3,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif"
  },
  {
    _id: "15",
    name: "Dee Jay",
    health: 50,
    attack: 5,
    defense: 5,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif"
  },
  {
    _id: "16",
    name: "T.Hawk",
    health: 60,
    attack: 5,
    defense: 3,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif"
  },
  {
    _id: "17",
    name: "Akuma",
    health: 70,
    attack: 5,
    defense: 5,
    source:
      "http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif"
  }
];

let nextId = DATA.length + 1;

const getUsers = () => {
  if (DATA.length !== 0) {
    return { status: "Ok", data: DATA };
  } else {
    return { status: "Error", error: "No users" };
  }
};

const getUserById = id => {
  const result = DATA.find(el => el._id === id);
  if (result) {
    return { status: "Ok", data: result };
  } else {
    return { status: "Error", error: "User not found" };
  }
};

const createUser = ({ name, health, attack, defense, source }) => {
  if (name && health && attack && defense && source) {
    const user = {
      _id: nextId.toString(),
      name,
      health,
      attack,
      defense,
      source
    };
    nextId += 1;
    DATA.push(user);
    return { status: "Ok", user };
  } else {
    return { status: "Error", error: "Adding user: properties missing" };
  }
};

const updateUser = (id, data) => {

  if (id) {
    const ind = DATA.findIndex(el => el._id === id);
    if (ind >= 0) {
      const user = DATA[ind];
      ["name", "health", "attack", "defense", "source"].map(prop => {
        if (data[prop]) {
          user[prop] = data[prop];
        }
      });

      return { status: "Ok", user };
    } else {
      return { status: "Error", error: "Updating user: wrong id" };
    }
  } else {
    return { status: "Error", error: "Updating user: properties missing" };
  }
};

const deleteUser = id => {
  if (id) {
    const ind = DATA.findIndex(el => el._id === id);

    if (ind >= 0) {
      DATA.splice(ind, 1);
      return { status: "Ok" };
    } else {
      return { status: "Error", error: "Deleting user: wrong user id" };
    }
  } else {
    return { status: "Error", error: "Deleting user: wrong user data" };
  }
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
};
