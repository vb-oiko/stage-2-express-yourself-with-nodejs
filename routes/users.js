var express = require("express");
var router = express.Router();

const {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser
} = require("../services/user.service");

const { isAuthorized } = require("../middlewares/auth.middleware");

router.get("/", function(req, res, next) {
  const result = getUsers();
  if (result.status === "Ok") {
    res.status(200).send(result);
  } else {
    res.status(404).send(result);
  }
});

router.get("/:id", function(req, res, next) {
  const result = getUserById(req.params.id);
  if (result.status === "Ok") {
    res.status(200).send(result);
  } else {
    res.status(404).send(result);
  }
});

router.post("/", isAuthorized, function(req, res, next) {
  const result = createUser(req.body);

  if (result.status === "Ok") {
    res.status(200).send(result);
  } else {
    res.status(400).send(result);
  }
});

router.put("/:id", (req, res, next) => {
  const id = req.params.id;
  const data = req.body;

  const result = updateUser(id, data);

  if (result.status === "Ok") {
    res.status(200).send(result);
  } else {
    res.status(400).send(result);
  }
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  const result = deleteUser(id);

  if (result.status === "Ok") {
    res.status(200).send(result);
  } else {
    res.status(400).send(result);
  }
});

module.exports = router;
