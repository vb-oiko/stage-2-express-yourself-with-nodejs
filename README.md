#### Web server for sample online fighting game

Endpoint: http://localhost:3000/

Headers:

- Content-Type: "application/json"
- Authorization: "admin"

You will find available requests types below.
In case of a success request you will get following response, where data holds the data you are requesting:

```
{
    "status": "Ok",
    "data": {... requested data ...}
}
```

If your request is incorrect you will get following response:

```
{
    "status": "Error",
    "error": "... error message ..."
}
```

In case if your request fails due to some other reason you will get a standard Express error web page.

- **GET**: _/user_
  get an array of all users

  sample response:

  ```
  {
    "status": "Ok",
    "data": [
        {
            "\_id": "1",
            "name": "Ryu",
            "health": 45,
            "attack": 4,
            "defense": 3,
            "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
        },
  ...

        {
            "_id": "2",
            "name": "Dhalsim",
            "health": 60,
            "attack": 3,
            "defense": 1,
            "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
        }
    ]
  }
  ```

- **GET**: _/user/:id_
  get one user by ID

  sample response:

  ```
  {
      "status": "Ok",
      "data": {
            "\_id": "5",
            "name": "Ken",
            "health": 45,
            "attack": 3,
            "defense": 4,
            "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif"
      }
  }
  ```

* **POST**: _/user_
  create user according to the data from the request body
  The properties _name, health, attack, defense, source_ are required. All other properties will be ignored. As a response you will get a user object with all the properties.
  sample request:

  ```
    {
        "name": "Fighter",
        "health":100,
        "attack": 3,
        "defense": 1,
        "source":
        "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif",
    }
  ```

  sample response:

  ```
    {
        "status": "Ok",
        "user": {
            "_id": "18",
            "name": "Fighter",
            "health": 100,
            "attack": 3,
            "defense": 1,
            "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
        }
    }
  ```

* **PUT**: _/user/:id_
  update user according to the data from the request body
  You can send new values to some or all of the properties _name, health, attack, defense, source_. All other properties will be ignored. As a response you will get a user object with all the properties.
  sample request:
  ```
    {
        "health":200,
    }
  ```
  sample response:
  ```
    {
        "status": "Ok",
        "user": {
            "_id": "18",
            "name": "Fighter",
            "health": 200,
            "attack": 3,
            "defense": 1,
            "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
        }
    }
  ```

- **DELETE**: _/user/:id_
  delete one user by ID
  sample response:
  ```
  {
    "status": "Ok"
  }
  ```
